from temperature import Temperature
class TemperatureDict:
  
  def __init__(self, hdd:list):
    self.hdd = hdd
    self.temperatures = []

  def addTemperature(self, temperature:Temperature):
    self.temperatures.append(temperature)
  
  def avg(self):
    if(0 == len(self.temperatures)):
      return 0
    sum = 0
    for temp in self.temperatures:
      sum += temp.temperature
    return sum / len(self.temperatures)
