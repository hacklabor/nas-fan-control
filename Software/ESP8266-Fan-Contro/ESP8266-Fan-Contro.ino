/*
 Name:		ESP8266_Fan_Contro.ino
 Created:	28.06.2019 07:30:49
 Author:	Thk
*/



#include <LOLIN_I2C_MOTOR.h>



long pwm_FG1 = 99;
long pwm_FG2 = 99;
unsigned long lastDataTime = 0;
unsigned long WDTimeout = 10000;
float temp(NAN), hum(NAN), pres(NAN);

String inputString = "";         // a String to hold incoming data
bool stringComplete = false;

//Motor shiled I2C Address: 0x30
//PWM frequency: 1000Hz(1kHz)
LOLIN_I2C_MOTOR motor; //I2C address 0x30

void setup() {
	Serial.begin(115200);
	Serial.println("#START");
	inputString.reserve(200);
	while (motor.PRODUCT_ID != PRODUCT_ID_I2C_MOTOR) //wait motor shield ready.
	{
		motor.getInfo();
	}
	motor.changeFreq(MOTOR_CH_BOTH, 80); //Change A & B 's Frequency to 1000Hz.
 
	motor.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CW);
	motor.changeStatus(MOTOR_CH_B, MOTOR_STATUS_CCW);
	
}

void loop() {


	while (Serial.available()) {
		// get incoming byte:
		char inChar = (char)Serial.read();
		// add it to the inputString:
		inputString += inChar;
		
		if (inChar == '\n') {
			ParseInString(inputString);

			// clear the string:
			inputString = "";
			stringComplete = false;
			lastDataTime = millis();
		}
	}
	if (millis() - lastDataTime > WDTimeout) {
		// DataTimeOut max fan speed
		pwm_FG1 = 100;
		pwm_FG2 = 100;

	}

	
		// set Fan speed
	motor.changeDuty(MOTOR_CH_A, pwm_FG1);
	motor.changeDuty(MOTOR_CH_B, pwm_FG2);

		
		delay(100);
	}


void ParseInString(String INSTR) {
	const uint8_t datenlaenge = 2;
	String TeilString[datenlaenge];
	uint8_t Zaehler = 0;
	if (INSTR.length() < 5) { return; }
	if (INSTR.substring(0, 2) != "##") { return; }
	
	for (int i = 2; i < INSTR.length() - 1; i++) {

		if (INSTR.substring(i, i + 1) == ";") {
			Zaehler++;
			if (Zaehler == datenlaenge) { break; }
		}
		else
		{
			TeilString[Zaehler] += INSTR.substring(i, i + 1);
		}
	}



	pwm_FG1 = TeilString[0].toInt();
	pwm_FG2 = TeilString[1].toInt();
	

	
	Serial.println("##" + String(pwm_FG1) + ";" + String(pwm_FG2)+";" + String(temp) + ";" + String(hum) + ";" + String(pres));


}

