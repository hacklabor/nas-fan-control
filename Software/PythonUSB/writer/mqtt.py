import paho.mqtt.client as mqttClient
import time
import os
class Mqtt:
    def __init__(self, server:str, topic:str, user:str, pw:str):
        self.server = server
        self.topic = topic
        self.client = mqttClient.Client()
        self.client.username_pw_set(user, pw)
        self.client.connect(self.server)
        self.client.on_message = self.onmessage
        self.client.subscribe("nas04", 0)
        self.client.loop_start()

    def write (self, text:str):
        #print (text)
        msg = self.client.publish(self.topic, text)
        msg.wait_for_publish()

    def onmessage(self, client, Data1, msg):
        print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
        print(msg.payload.decode())
        MqttMsgData = msg.payload.decode()

        if MqttMsgData == "OFF":
            os.system("sudo shutdown -h now")


    def __del__(self):
        self.client.disconnect()
