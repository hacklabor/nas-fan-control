import socket
from temperature import Temperature
import time

class HDD:
      
    def __init__(self, host="127.0.0.1", port=7634):
        self.host = host
        self.port = port

    def read(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.host,self.port))

        data = ""

        while True:
            dataSocket = s.recv(1024)
            if not dataSocket: break
            data += dataSocket.decode()

        s.close()

        #print(data)
        temperatures = []
        #test string
        #data = "|/dev/sda|SanDisk SD7SB3Q256G1002|32|C||/dev/sdh|SanDisk SD7SB3Q256G1002|32|C|"
        data = str(data)
        #print(data)
        data = data.replace("||", "|\n|")
        #print (data)
        #return []
        for hdd in data.splitlines():

            hddData = hdd.split("|")   
            #print (hddData)
            temp = Temperature(hddData[1], hddData[3])
            temperatures.append(temp)
        return temperatures
