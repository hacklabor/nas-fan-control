# Nas Fan Control

PWM fan speed control of the NAS


## Esp8266 serial controlstring

* serial String = "##{PWM_L};{PWM_R}" 
* "##" = Startchar
* {PWM_L} as string ("000") = power  left fan Group 0-100%
* {PWM_R} as string("000")= power rigth fan group 0-100%


## electrical components

* WEMOS mini Esp8266 
* WEMOS mini motorshield
* 4x fan wire extension
* 1x Y IDE Power cable 5.25'

![Drawing](/pictures/NasFanControl.png "") 