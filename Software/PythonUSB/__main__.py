from reader.hdd import HDD
from temperatureDict import TemperatureDict
from writer.mqtt import Mqtt
import serial
import os
import socket
import secr
import time

desiredHddTemp = 50
pTemp = 15  # proportional PI Controller
iTemp = 0.4 #integral PI Controller



def calc_pwm(act_temp, desired_temp, sum_tmp, ptmp, itmp):

    diff = act_temp - desired_temp
    sum_tmp += diff
    p_diff = diff * ptmp
    i_diff = sum_tmp * itmp

    fan_speed = p_diff + i_diff
    fan_speed = min(100, fan_speed)
    fan_speed = max(15, fan_speed)

    sum_tmp = min(100, sum_tmp)
    sum_tmp = max(-100, sum_tmp)

    return [fan_speed, sum_tmp]


def get_hd_temp(hard_disk):

    hdd_sums = dict()
    hdd_sums["left"] = TemperatureDict(["/dev/sdb", "/dev/sdc", "/dev/sdd"])
    hdd_sums["middle"] = TemperatureDict(["/dev/sde", "/dev/sdf"])
    hdd_sums["right"] = TemperatureDict(["/dev/sdg", "/dev/sdh", "/dev/sdi"])

    for hdd in hard_disk:
        #print (hdd.name)
        for pos,tempDict in hdd_sums.items():
            #print(position)
            if hdd.name in tempDict.hdd:
                tempDict.addTemperature(hdd)
    act_left = max(hdd_sums["left"].avg(), hdd_sums["middle"].avg())
    act_right = max(hdd_sums["right"].avg(), hdd_sums["middle"].avg())
    print("actualLeft" + str(act_left))
    print("actualRight" + str(act_right))
    return [act_left, act_right]


def send_serial(l_pwm, r_pwm):

        serial_string = "##" + str(l_pwm) + ";" + str(r_pwm)+"\n"
        serial_string = f"##{str(l_pwm)};{str(r_pwm)}\n"

        try:
            with serial.Serial(secr.Serialtty, 115200, timeout=1) as ser:
                ser.write(serial_string.encode())
                print("TX" + serial_string)
                return ser.readline()
        except Exception as e:
            print(str(e))


def send_mqtt (HardDisk, mqttWriter):
    for hd in HardDisk:
        mqtt_string = "sensors,host="+socket.gethostname()
        mqtt_string += ",category=temperature,sensor="
        mqtt_string += hd.name+",unit=°C value="
        mqtt_string += str(hd.temperature)
        mqttWriter.write(mqtt_string)


if __name__ == "__main__":

    mqtt_writer = Mqtt(secr.MqttServer, secr.Topic,secr.MqttUser,secr.MqttPw)

    pi_controller_left = [0, 0]  # pwm, summe for Integral
    pi_controller_right = [0, 0] # pwm, summe for Integral
    actual_temp = [1, 1] #
    mqttTime = time.time()

    while True:
        try:
            hddReader = HDD()
            hardDrives = hddReader.read()
            actual_temp = get_hd_temp(hardDrives)
        except:
            print("hddReader error")

        if mqttTime + 60 < time.time():
            mqttTime = time.time()
            try:
                send_mqtt(hardDrives, mqtt_writer)
            except:
                print("send_mqtt error")

        #calc PI controller PWM 0-100% left side
        pi_controller_left = calc_pwm(actual_temp[0], desiredHddTemp, pi_controller_left[1], pTemp, iTemp)

        # calc PI controller PWM 0-100% right side
        pi_controller_right = calc_pwm(actual_temp[1], desiredHddTemp, pi_controller_right[1], pTemp, iTemp)

        #send over serial to esp8266
        response = send_serial(int(pi_controller_left[0]), int(pi_controller_right[0]))

        #response b'##100;28;25.77;46.06;101966.61\r\n' pwm_left; pwm_right; temp [°C]; huminity [%]; airpressure [Pa]
        print(response)
        time.sleep(0.5)
